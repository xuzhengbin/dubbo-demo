package com.alibaba.dubbo.demo.provider;

/**
 * @Author: xuzhengbin
 * @Description
 * @Date: 2018/6/14
 * @Modified By:
 */
public interface DemoService {
    String sayHello(String name);
}
